package todo_back

import (
	"log"
	"net/http"
)

type Server struct {
	HttpServer *http.Server
}

func (s *Server) Listen(port string) {
	s.HttpServer = &http.Server{ Addr: ":" + port }

	log.Fatal(s.HttpServer.ListenAndServe())
}